// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  AppSettingServiceHost: 'http://localhost:3000/',
  LoggingServiceHost: 'http://localhost:3001/',
  NotesServiceHost: 'http://localhost:3002/',
  ResourcesServiceHost: 'http://localhost:3003/',
  SFDCServiceHost: 'http://localhost:3004/',
  TasksServiceHost: 'http://localhost:3005/',
  TimeSheetServiceHost: 'http://localhost:3006/',
  UsersServiceHost: 'http://localhost:3007/',
  envName: 'dev'
};
