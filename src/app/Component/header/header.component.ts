import { Component, OnInit } from '@angular/core';
import { Global} from '../../global';
import {Response} from '@angular/http';
import {AppSettingsService} from '../../Services/app-settings.service';
import {TranslateService} from '@ngx-translate/core';

@Component ({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [AppSettingsService]
})
export class HeaderComponent implements OnInit {
  tour = false;
  environmentData: any[] = [];
  defaultEnvironmentColor:string;
  constructor(public global: Global,
              public _appSettingsService: AppSettingsService,
              private translate: TranslateService) {
    localStorage.setItem('tour', '0');
    this.tour = false;
  }
  onStartTour() {
   localStorage.setItem('tour', '1');
   localStorage.setItem('tourClass', 'tour')
   localStorage.setItem('body-class', 'greybg')
   this.tour = true;
  }
  switchLanguage(event) {
    this.translate.use(event.target.value);
  }
  onEndTour() {
    localStorage.setItem('tour', '0')
    localStorage.setItem('tourClass', '')
    localStorage.setItem('body-class', '')
    this.tour = false;
  }
  ngOnInit() {
    this.getAllEnvironments();
  }
  getAllEnvironments() {
    this._appSettingsService.getAllEnvironments()
      .subscribe(
        (data: Response) => {
          this.environmentData = data.json();
          this.defaultEnvironmentColor = this.environmentData[0].rgbColor;
        },
        (error) => {
        }
      );
  }
}
