import {Component, OnInit} from '@angular/core';
import { TourService } from 'ngx-tour-ng-bootstrap';
import { Global} from '../../global';
import { Response } from '@angular/http';
import { ResourcesService } from '../../Services/resources.service';
import {SfdcService} from '../../Services/sfdc.service';
import { TasksService } from '../../Services/tasks.service';
import { UsersService } from '../../Services/users.service';
import { TranslateService} from '@ngx-translate/core';
import { TranslationHelper } from '../../Services/translation-helper.service'

// import models
import { Config } from '../../Models/config.model';
import { CurrentEvent } from '../../Models/current-event.model';
import { OpCode } from '../../Models/opcode.model';
import { Resources } from '../../Models/resources.model';
import { Tasks } from '../../Models/tasks.model';
import { OperatorModel } from '../../Models/operator.model'


@Component ({
  selector: 'app-main-screen',
  templateUrl: './main-screen.component.html',
  styleUrls: ['./main-screen.component.css'],
  providers: [ResourcesService, SfdcService, UsersService, TasksService, TranslationHelper]
})

export class MainScreenComponent implements OnInit {
  ResourceModel: Array<Resources>;
  ConfigModel: Array<Config>;
  OpCodeModel: Array<OpCode>;
  CurrentEventModel = new CurrentEvent();
  OperatorModel = new OperatorModel();
  TaskModel = new Tasks();
  resourceData: any[] = [];
  currentEventData: any[] = [];
  operatorData: any[] = [];
  taskData: any[] = [];
  opCodeData: any[] = [];
  configData: any[] = [];
  settingsData: any[] = [];
  activeResourceId: number;
  idleResourceId: number;
  activeResource = '';
  idleResource = '';
  activeResourceOpCode = '';
  idleResourceOpCode = '';
  activeResourceOpCodeBg = '';
  idleResourceOpCodeBg = '';
  activeOpCodeBackground = '';
  opCodeTypeName = '';
  configDescription = '';
  opCodeBackGround = '';
  operator = '';
  r1 = 'active';
  r2 = 'idle';
  operatorBg = '';
  plannedQtyPercent: any = '0%';
  plannedQtyNumber: any = 0;
  defaultEnvironmentColor:string;
  runhour = 0;
  runmin = 0;
  isNaN: Function = Number.isNaN;
  constructor(public tourService: TourService,
              public _resourcesService: ResourcesService,
              public _sfdcService: SfdcService,
              public _usersService: UsersService,
              public _tasksService: TasksService,
              public global: Global,
              private translate: TranslateService,
              public _translationHelperService: TranslationHelper) {
  }

  ngOnInit() {
    this.getConfiguredResources();
  }
  getConfiguredResources() {
      this._resourcesService.getConfiguredResources()
      .subscribe(
        (data: Response) => {
          this.ResourceModel = data.json();
          this.activeResource = this.ResourceModel[0].resourceName;
          this.activeResourceId = this.ResourceModel[0].resourceID;
          if(this.ResourceModel.length==2){
          this.idleResourceId = this.ResourceModel[1].resourceID;
          this.idleResource = this.ResourceModel[1].resourceName;
        }
        },
        (error) => {
        },
        () => {
          let id = this.activeResourceId;
          this._sfdcService.getCurrentEventById(id)
          .subscribe(
            (data: Response) => {
              this.currentEventData = data.json()
              this.CurrentEventModel = data.json()[0];
            },
            (Error) => {

            },
            () => {
              this._usersService.getOperator(this.CurrentEventModel.operatorId)
              .subscribe(
                (data: Response) => {
                  this.OperatorModel = data.json()[0];
                  if (this.OperatorModel == undefined) {
                    this.operator = 'Unknown';
                  }
                  else {
                    this.operator = this.OperatorModel.lastName + ',' + this.OperatorModel.firstName[0];
                  }
                },
                (Error) => {
    
                },
                () => {
                  this._tasksService.getTask(this.CurrentEventModel.taskId)
                  .subscribe(
                    (data: Response) => {
                      this.TaskModel = data.json()[0];
                    },
                    (Error) => {
        
                    },
                    () => {
                      if(this.TaskModel == undefined) {
                        this.TaskModel = new Tasks()
                        this.CurrentEventModel.assistants = 0;
                        this.TaskModel.noUp = 0;
                        this.TaskModel.plannedCopies = 0;
                        this.TaskModel.qtyToGo = 0;
                        this.TaskModel.wasteCopies = 0;
                        this.plannedQtyNumber = 0;
                        this.plannedQtyPercent = '0%';
                        this.TaskModel.subJobNo = '';
                        this.TaskModel.company = '';
                        this.TaskModel.taskName = '';
                        this.TaskModel.runMins = 0;
                        this.runhour = 0;
                        this.runmin = 0;
                    } else
                    if (this.TaskModel.taskStatus == 'Lifted') {
                      this.CurrentEventModel.assistants = 0;
                      this.TaskModel.noUp = 0;
                      this.TaskModel.plannedCopies = 0;
                      this.TaskModel.qtyToGo = 0;
                      this.TaskModel.wasteCopies = 0;
                      this.plannedQtyNumber = 0;
                      this.plannedQtyPercent = '0%';
                      this.TaskModel.subJobNo = '';
                      this.TaskModel.company = '';
                      this.TaskModel.taskName = '';
                      this.TaskModel.runMins = 0;
                      this.runhour = 0;
                      this.runmin = 0;
                    }
                    this.runhour = Math.round(this.TaskModel.runMins/60);
                    this.runmin = Math.round(this.TaskModel.runMins%60);
                        this.plannedQtyPercent = Math.round(((this.TaskModel.plannedCopies - this.TaskModel.qtyToGo)/this.TaskModel.plannedCopies)*100);
                        if (isNaN(this.plannedQtyPercent)){
                          this.plannedQtyNumber = 0;
                          this.plannedQtyPercent = '0%'
                        }
                        else {
                          this.plannedQtyNumber = this.plannedQtyPercent > 100 ? 100 : this.plannedQtyPercent;
                          this.plannedQtyPercent = this.plannedQtyPercent + '%';
                        }
                        this.TaskModel.runMins = Math.round(this.TaskModel.runMins);
                        localStorage.setItem('resourceData',JSON.stringify(this.ResourceModel))
                        localStorage.setItem('currentEventData',JSON.stringify(this.CurrentEventModel))
                        localStorage.setItem('operatorData',JSON.stringify(this.OperatorModel))
                        localStorage.setItem('taskData',JSON.stringify(this.TaskModel))
                        this._resourcesService.getOpCodes(this.activeResourceId)
                        .subscribe(
                          (data: Response) => {
                            this.OpCodeModel = data.json();
                            this.activeResourceOpCode = this.OpCodeModel.find(x => x.opCodeID == this.currentEventData[0].opCodeId).opCodeTypeName;
                            this.activeResourceOpCodeBg = this.OpCodeModel.find(x => x.opCodeID == this.currentEventData[0].opCodeId).opCodeTypeColorRGB;
                            this.opCodeTypeName = this.OpCodeModel.find(x => x.opCodeID == this.currentEventData[0].opCodeId).opCodeTypeName;
                            this.opCodeBackGround = this.OpCodeModel.find(x => x.opCodeID == this.currentEventData[0].opCodeId).opCodeTypeColorRGB;
                            localStorage.setItem('opCodeData',JSON.stringify(this.OpCodeModel))
                          },
                          (Error) => {
                               
                          },
                          () => {
                              if (this.operator == 'Unknown' && this.opCodeTypeName != 'Idle'){
                                this.operatorBg = 'operator-bg';
                              }
                              else {
                                this.operatorBg = '';
                              }
                              if(this.ResourceModel.length == 2){
                                this._sfdcService.getCurrentEventById(this.idleResourceId)
                                .subscribe(
                                  (data: Response) => {
                                    let opCodeId = data.json()[0].opCodeId;
                                    this.idleResourceOpCode = this.OpCodeModel.find(x => x.opCodeID == opCodeId).opCodeTypeName;
                                    this.idleResourceOpCodeBg = this.OpCodeModel.find(x => x.opCodeID == opCodeId).opCodeTypeColorRGB;
                                  },
                                  (Error) => {
                      
                                  })
                              }
                          }
                        )
                        this._resourcesService.getConfigs(this.activeResourceId)
                        .subscribe(
                          (data: Response) => {
                            this.ConfigModel = data.json();
                            this.configDescription = this.ConfigModel.find(x => x.configId == this.currentEventData[0].configId).configDescription;
                            localStorage.setItem('configData',JSON.stringify(this.ConfigModel))
                          },
                          (Error) => {
              
                          }
                        )
                        this._resourcesService.getSettings(this.activeResourceId)
                        .subscribe(
                          (data: Response) => {
                            this.settingsData = data.json();
                            let language = this.settingsData.find(x =>x.settingID == 10).SettingValue;
                            this.translate.use(language);
                            this._translationHelperService.getTranslatedValue();
                            localStorage.setItem('settingsData',JSON.stringify(this.opCodeData))
                          },
                          (Error) => {
              
                          }
                        )
                      }
                  )
                }
              )
            }
          )
        }
      );
    //}  
  }

  switchResource(resourceID) {
        this._sfdcService.getCurrentEventById(resourceID)
        .subscribe(
          (data: Response) => {
            this.currentEventData = data.json()
            this.CurrentEventModel = data.json()[0];
          },
          (Error) => {

          },
          () => {
            this._usersService.getOperator(this.CurrentEventModel.operatorId)
            .subscribe(
              (data: Response) => {
                this.OperatorModel = data.json()[0];
                if (this.OperatorModel == undefined) {
                  this.operator = 'Unknown';
                }
                else {
                  this.operator = this.OperatorModel.lastName + ',' + this.OperatorModel.firstName[0];
                }
              },
              (Error) => {
  
              },
              () => {
                this._tasksService.getTask(this.CurrentEventModel.taskId)
                .subscribe(
                  (data: Response) => {
                    this.TaskModel = data.json()[0];
                  },
                  (Error) => {
      
                  },
                  () => {
                    if(this.TaskModel == undefined) {
                      this.TaskModel = new Tasks()
                      this.CurrentEventModel.assistants = 0;
                      this.TaskModel.noUp = 0;
                      this.TaskModel.plannedCopies = 0;
                      this.TaskModel.qtyToGo = 0;
                      this.TaskModel.wasteCopies = 0;
                      this.plannedQtyNumber = 0;
                      this.plannedQtyPercent = '0%';
                      this.TaskModel.subJobNo = '';
                      this.TaskModel.company = '';
                      this.TaskModel.taskName = '';
                      this.TaskModel.runMins = 0;
                      this.runhour = 0;
                      this.runmin = 0;
                  } else
                  if (this.TaskModel.taskStatus == 'Lifted') {
                    this.CurrentEventModel.assistants = 0;
                    this.TaskModel.noUp = 0;
                    this.TaskModel.plannedCopies = 0;
                    this.TaskModel.qtyToGo = 0;
                    this.TaskModel.wasteCopies = 0;
                    this.plannedQtyNumber = 0;
                    this.plannedQtyPercent = '0%';
                    this.TaskModel.subJobNo = '';
                    this.TaskModel.company = '';
                    this.TaskModel.taskName = '';
                    this.TaskModel.runMins = 0;
                    this.runhour = 0;
                    this.runmin = 0;
                  }
                  this.runhour = Math.round(this.TaskModel.runMins/60);
                  this.runmin = Math.round(this.TaskModel.runMins%60);
                    this.plannedQtyPercent = Math.round(((this.TaskModel.plannedCopies - this.TaskModel.qtyToGo)/this.TaskModel.plannedCopies)*100);
                    if (isNaN(this.plannedQtyPercent)){
                      this.plannedQtyNumber = 0;
                      this.plannedQtyPercent = '0%'
                    }
                    else {
                      this.plannedQtyNumber = this.plannedQtyPercent > 100 ? 100 :  this.plannedQtyPercent ;
                      this.plannedQtyPercent = this.plannedQtyPercent + '%';
                    }
                    this.TaskModel.runMins = Math.round(this.TaskModel.runMins);
                    localStorage.setItem('resourceData',JSON.stringify(this.ResourceModel))
                    localStorage.setItem('currentEventData',JSON.stringify(this.CurrentEventModel))
                    localStorage.setItem('operatorData',JSON.stringify(this.OperatorModel))
                    localStorage.setItem('taskData',JSON.stringify(this.TaskModel))
                    this._resourcesService.getOpCodes(this.activeResourceId)
                    .subscribe(
                      (data: Response) => {
                        this.OpCodeModel = data.json();
                        this.opCodeTypeName = this.OpCodeModel.find(x => x.opCodeID == this.currentEventData[0].opCodeId).opCodeTypeName;
                        this.opCodeBackGround = this.OpCodeModel.find(x => x.opCodeID == this.currentEventData[0].opCodeId).opCodeTypeColorRGB;
                        localStorage.setItem('opCodeData',JSON.stringify(this.OpCodeModel))
                      },
                      (Error) => {
                           
                      },
                      () => {
                        if (this.operator == 'Unknown' && this.opCodeTypeName != 'Idle'){
                          this.operatorBg = 'operator-bg';
                        }
                        else {
                          this.operatorBg = '';
                        }
                        
                      }
                    )
                    this._resourcesService.getConfigs(this.activeResourceId)
                    .subscribe(
                      (data: Response) => {
                        this.ConfigModel = data.json();
                        this.configDescription = this.ConfigModel.find(x => x.configId == this.currentEventData[0].configId).configDescription;
                        localStorage.setItem('configData',JSON.stringify(this.ConfigModel))
                      },
                      (Error) => {
          
                      }
                    )
                    this._resourcesService.getSettings(this.activeResourceId)
                    .subscribe(
                      (data: Response) => {
                        this.settingsData = data.json();
                        let language = this.settingsData.find(x =>x.settingID == 10).SettingValue;
                        this.translate.use(language);
                        this._translationHelperService.getTranslatedValue();
                        localStorage.setItem('settingsData',JSON.stringify(this.opCodeData))
                      },
                      (Error) => {
          
                      }
                    )
                  }
                )
              }
            )
          }
        );
  }
  onStartAt(event,resourceId, resourceClass) {
    if (localStorage.getItem('tour') === '1') {
      this.tourService.startAt(event.target.getAttribute('data-step-id'));
    }
    else {
    if (resourceId != undefined) {
          // switch resources
          if(resourceClass == 'idle') {
          let activeResourceId = this.activeResourceId;
          this.activeResourceId = this.idleResourceId;
          this.idleResourceId = activeResourceId;
          // switch resource classes
          let activeClass = this.r1;
          this.r1 = this.r2;
          this.r2 = activeClass;
          this.switchResource(this.activeResourceId);
          }
      }
    }
  }

  onViewBlur() {
    this.tourService.end();
  }

  onResourceChange(resourceId, resourceClass) {
    if(resourceId!=undefined){
        // switch resources
        if(resourceClass == 'idle') {
        let activeResourceId = this.activeResourceId;
        this.activeResourceId = this.idleResourceId;
        this.idleResourceId = activeResourceId;
        // switch resource classes
        let activeClass = this.r1;
        this.r1 = this.r2;
        this.r2 = activeClass;
        this.switchResource(this.activeResourceId);
      }
    }
  }
}
