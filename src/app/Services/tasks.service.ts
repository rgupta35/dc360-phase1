import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import { environment } from 'environments/environment';

@Injectable()
export class TasksService {
  constructor(private http: Http ) {}

  getTask(id: number) {
    return this.http.get(environment.TasksServiceHost + 'tasks?taskID=' + id)
  }
}
