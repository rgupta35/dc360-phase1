import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import { environment } from 'environments/environment';

@Injectable()
export class ResourcesService {
  macAddress = '00:14:22:01:45:23';
  constructor(private http: Http ) {
    localStorage.setItem('macAddress','00:14:22:01:45:23')
  }

  getConfiguredResources() {
    return this.http.get(environment.ResourcesServiceHost + 'machineConfig?pcPrimaryMacAddress=' + this.macAddress);
  }
  getOpCodes(resourceId) {
    return this.http.get(environment.ResourcesServiceHost + 'resources/' + resourceId + '/opCodes');
  }
  getConfigs(resourceId) {
    return this.http.get(environment.ResourcesServiceHost + 'resources/' + resourceId + '/configs');
  }
  getSettings(resourceId) {
    return this.http.get(environment.ResourcesServiceHost + 'resources/' + resourceId + '/settings');
  }
}
