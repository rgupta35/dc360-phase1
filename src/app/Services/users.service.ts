import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import { environment } from 'environments/environment';

@Injectable()
export class UsersService {
  constructor(private http: Http ) {}

  getOperator(id: number) {
    return this.http.get(environment.UsersServiceHost + 'operators?id=' + id)
  }
}
