import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import { environment } from 'environments/environment';

@Injectable()
export class SfdcService {
  macAddress = '00:14:22:01:45:24';
  constructor(private http: Http ) {}

  getCurrentEvent() {
    return this.http.get(environment.SFDCServiceHost + 'getCurrentEvent')
  }
  getCurrentEventById(id) {
    return this.http.get(environment.SFDCServiceHost + 'getCurrentEvent?resourceId='+id)
  }
}
