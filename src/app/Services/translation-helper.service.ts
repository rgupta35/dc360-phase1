import { TranslateService} from '@ngx-translate/core';
import { TourService } from 'ngx-tour-ng-bootstrap';
import {Injectable} from '@angular/core';
import { UrlSegment } from '@angular/router';
import { IStepOption} from '../Models/step-option';

@Injectable()
export class TranslationHelper {
    Option: IStepOption[] = [{
        anchorId: 'welcome',
        stepId: '0',
        content: 'Welcome to count adjustment screen tour. Click continue to move ahead or click cancel to stop tour here.',
        placement: 'below',
        title: 'Welcome',
        isClickable: false,
        titleKey: 'GuidedHelpTitleGroup.welcome',
        contentKey: 'GuidedHelpContentGroup.welcome'
      }, {
        anchorId: 'first',
        stepId: '1',
        content: 'Use keypad to enter adjustment value.',
        placement: 'below',
        title: 'Enter Adjustment Value',
        isClickable: false,
        titleKey: 'GuidedHelpTitleGroup.first',
        contentKey: 'GuidedHelpContentGroup.first'
      }, {
        anchorId: 'second',
        stepId: '2',
        content: 'Select a tranfer type from here.',
        placement: 'below',
        title: 'Select Transfer Type',
        isClickable: false,
        titleKey: 'GuidedHelpTitleGroup.second',
        contentKey: 'GuidedHelpContentGroup.second'
      }, {
        anchorId: 'third',
        stepId: '3',
        content: 'Click on apply to see Good Sheet, Waste Sheet and Adjustment Sheet changes',
        placement: 'above',
        title: 'Apply',
        isClickable: false,
        titleKey: 'GuidedHelpTitleGroup.third',
        contentKey: 'GuidedHelpContentGroup.third'
      }, {
          anchorId: 'fourth',
          stepId: '4',
          content: 'Click on Close to reset screen.',
          placement: 'above',
          title: 'Clear',
          isClickable: false,
          titleKey: 'GuidedHelpTitleGroup.fourth',
          contentKey: 'GuidedHelpContentGroup.fourth'
      }, {
        anchorId: 'subJobNo',
        stepId: '5',
        content: 'Job number',
        placement: 'above',
        title: 'Job number',
        isClickable: false,
        titleKey: 'GuidedHelpTitleGroup.subJobNo',
        contentKey: 'GuidedHelpContentGroup.subJobNo' 
      }, {
        anchorId: 'company',
        stepId: '6',
        content: 'Company',
        placement: 'below',
        title: 'Company',
        isClickable: false,
        titleKey: 'GuidedHelpTitleGroup.company',
        contentKey: 'GuidedHelpContentGroup.company'
      }, {
        anchorId: 'taskName',
        stepId: '7',
        content: 'Title',
        placement: 'above',
        title: 'Title',
        isClickable: false,
        titleKey: 'GuidedHelpTitleGroup.taskName',
        contentKey: 'GuidedHelpContentGroup.taskName'
      }, {
        anchorId: 'good',
        stepId: '8',
        content: 'Good Sheets',
        placement: 'above',
        title: 'Good Sheets',
        isClickable: false,
        titleKey: 'GuidedHelpTitleGroup.good',
        contentKey: 'GuidedHelpContentGroup.good' 
      }, {
        anchorId: 'waste',
        stepId: '9',
        content: 'Waste Sheets',
        placement: 'above',
        title: 'Waste Sheets',
        isClickable: false,
        titleKey: 'GuidedHelpTitleGroup.waste',
        contentKey: 'GuidedHelpContentGroup.waste' 
      }, {
        anchorId: 'adjustment',
        stepId: '10',
        content: 'Adjustment Sheets',
        placement: 'above',
        title: 'Adjustment Sheets',
        isClickable: false,
        titleKey: 'GuidedHelpTitleGroup.adjustment',
        contentKey: 'GuidedHelpContentGroup.adjustment' 
      }, {
        anchorId: 'quantity',
        stepId: '11',
        content: 'Quantity to Go',
        placement: 'above',
        title: 'Quantity to Go',
        isClickable: false,
        titleKey: 'GuidedHelpTitleGroup.quantity',
        contentKey: 'GuidedHelpContentGroup.quantity'
      },
        {
          anchorId: 'idle',
          stepId: '12',
          content: 'Resource status is Idle',
          placement: 'below',
          title: 'Resource Status',
          isClickable: false,
          titleKey: 'GuidedHelpTitleGroup.Idle',
          contentKey: 'GuidedHelpContentGroup.idle' 
      },
        {
          anchorId: 'opcode',
          stepId: '13',
          content: 'OpCode Id',
          placement: 'below',
          title: 'OpCode Id',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.opcode',
          contentKey: 'GuidedHelpContentGroup.opcode' 
        },
        {
          anchorId: 'operator',
          stepId: '14',
          content: 'Operator',
          placement: 'below',
          title: 'Operator',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.operator',
          contentKey: 'GuidedHelpContentGroup.operator' 
        },
        {
          anchorId: 'assets',
          stepId: '15',
          content: 'Assets',
          placement: 'below',
          title: 'Assets',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.assets',
          contentKey: 'GuidedHelpContentGroup.assets'
        },
        {
          anchorId: 'running',
          stepId: '16',
          content: 'Resource status is Running',
          placement: 'below',
          title: 'Resource Status',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.running',
          contentKey: 'GuidedHelpContentGroup.running'
        },
        {
          anchorId: 'plannedqty',
          stepId: '17',
          content: 'Planned Quantity',
          placement: 'below',
          title: 'Planned Quantity',
          isClickable: false,
          titleKey: 'GuidedHelpTitleGroup.plannedqty',
          contentKey: 'GuidedHelpContentGroup.plannedqty' 
        },
        {
          anchorId: 'qtytogo',
          stepId: '18',
          content: 'Quantity to Go',
          placement: 'below',
          title: 'Quantity to Go',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.qtytogo',
          contentKey: 'GuidedHelpContentGroup.qtytogo' 
        },
        {
          anchorId: 'waste',
          stepId: '19',
          content: 'Waste',
          placement: 'below',
          title: 'Waste',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.waste',
          contentKey: 'GuidedHelpContentGroup.waste' 
        },
        {
          anchorId: 'wipandproducedqty',
          stepId: '20',
          content: 'WIP & Produced Qty',
          placement: 'below',
          title: 'WIP & Produced Qty',
          isClickable: false,
          titleKey: 'GuidedHelpTitleGroup.wipandproducedqty',
          contentKey: 'GuidedHelpContentGroup.wipandproducedqty'
        },
        {
          anchorId: 'company',
          stepId: '21',
          content: 'Company',
          placement: 'below',
          title: 'Company',
          isClickable: false,
          titleKey: 'GuidedHelpTitleGroup.company',
          contentKey: 'GuidedHelpContentGroup.company'
        },
        {
          anchorId: 'title',
          stepId: '22',
          content: 'Title',
          placement: 'below',
          title: 'Title',
          isClickable: false,
          titleKey: 'GuidedHelpTitleGroup.title',
          contentKey: 'GuidedHelpContentGroup.title' 
        },
        {
          anchorId: 'noup',
          stepId: '23',
          content: 'Number of Ups',
          placement: 'below',
          title: 'Number of Ups',
          isClickable: false,
          titleKey: 'GuidedHelpTitleGroup.noup',
          contentKey: 'GuidedHelpContentGroup.noup' 
        },
        {
          anchorId: 'jobticket',
          stepId: '24',
          content: 'Job Ticket',
          placement: 'below',
          title: 'Job Ticket',
          isClickable: false,
          titleKey: 'GuidedHelpTitleGroup.jobticket',
          contentKey: 'GuidedHelpContentGroup.jobticket'
        },
        {
          anchorId: 'qty',
          stepId: '25',
          content: 'Quantity',
          placement: 'below',
          title: 'Quantity',
          isClickable: false,
          titleKey: 'GuidedHelpTitleGroup.qty',
          contentKey: 'GuidedHelpContentGroup.qty'
        },
        {
          anchorId: 'btnview',
          stepId: '26',
          content: 'Go to Job Details Screen',
          placement: 'below',
          title: 'View Job Details',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.btnview',
          contentKey: 'GuidedHelpContentGroup.btnview'
        },
        {
          anchorId: 'jobno',
          stepId: '27',
          content: 'Job Number',
          placement: 'below',
          title: 'Job Number',
          isClickable: false,
          titleKey: 'GuidedHelpTitleGroup.jobno',
          contentKey: 'GuidedHelpContentGroup.jobno'
        },
        {
          anchorId: 'assets0',
          stepId: '28',
          content: 'Assistants',
          placement: 'below',
          title: 'Assistants',
          isClickable: false,
          titleKey: 'GuidedHelpTitleGroup.assets',
          contentKey: 'GuidedHelpContentGroup.assets'
        },
        {
          anchorId: 'qtytogo0',
          stepId: '29',
          content: 'Qty to Go',
          placement: 'below',
          title: 'Qty to Go',
          isClickable: false,
          titleKey: 'GuidedHelpTitleGroup.qtytogo',
          contentKey: 'GuidedHelpContentGroup.qtytogo'
        },
        {
          anchorId: 'waste0',
          stepId: '30',
          content: 'Waste',
          placement: 'below',
          title: 'Waste',
          isClickable: false,
          titleKey: 'GuidedHelpTitleGroup.waste',
          contentKey: 'GuidedHelpContentGroup.waste'
        },
        {
          anchorId: 'primaryresource',
          stepId: '31',
          content: 'Resource One',
          placement: 'below',
          title: 'Resource One',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.primaryresource',
          contentKey: 'GuidedHelpContentGroup.primaryresource'
        },
        {
          anchorId: 'secondaryresource',
          stepId: '32',
          content: 'Resource Two',
          placement: 'below',
          title: 'Resource Two',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.secondaryresource',
          contentKey: 'GuidedHelpContentGroup.secondaryresource'
        },
        {
          anchorId: 'createwipicon',
          stepId: '33',
          content: 'Create WIP Pallet',
          placement: 'above',
          title: 'Create WIP Pallet',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.createwipicon',
          contentKey: 'GuidedHelpContentGroup.createwipicon'
        },
        {
          anchorId: 'consumewipicon',
          stepId: '34',
          content: 'Consume WIP/FG Pallet',
          placement: 'above',
          title: 'Consume WIP/FG Pallet',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.consumewipicon',
          contentKey: 'GuidedHelpContentGroup.consumewipicon'
        },
        {
          anchorId: 'searchwipicon',
          stepId: '35',
          content: 'Search WIP/FG Inventory',
          placement: 'above',
          title: 'Search WIP/FG Inventory',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.searchwipicon',
          contentKey: 'GuidedHelpContentGroup.searchwipicon'
        },
        {
          anchorId: 'notesicon',
          stepId: '36',
          content: 'Notes',
          placement: 'above',
          title: 'Notes',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.notesicon',
          contentKey: 'GuidedHelpContentGroup.notesicon'
        },
        {
          anchorId: 'stopicon',
          stepId: '37',
          content: 'Stop',
          placement: 'above',
          title: 'Stop',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.stopicon',
          contentKey: 'GuidedHelpContentGroup.stopicon'
        },
        {
          anchorId: 'joblisticon',
          stepId: '38',
          content: 'Job list',
          placement: 'above',
          title: 'Job list',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.joblisticon',
          contentKey: 'GuidedHelpContentGroup.joblisticon'
        },
        {
          anchorId: 'consumerawmaterialicon',
          stepId: '39',
          content: 'Consume Raw Materials',
          placement: 'above',
          title: 'Consume Raw Materials',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.consumerawmaterialicon',
          contentKey: 'GuidedHelpContentGroup.consumerawmaterialicon'
        },
        {
          anchorId: 'changelanguageicon',
          stepId: '40',
          content: 'Change Language',
          placement: 'above',
          title: 'Change Language',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.changelanguageicon',
          contentKey: 'GuidedHelpContentGroup.changelanguageicon'
        },
        {
          anchorId: 'adminicon',
          stepId: '41',
          content: 'Admin',
          placement: 'above',
          title: 'Admin',
          isClickable: true,
          titleKey: 'GuidedHelpTitleGroup.adminicon',
          contentKey: 'GuidedHelpContentGroup.adminicon'
        }
      ]
    constructor(private translate: TranslateService,
        public tourService: TourService) {
}
    getTranslatedValue() {
        for (let i= 0; i < this.Option.length; i++) 
        {
          // this.Option[i].title = 'title' + i;
          this.translate.get(this.Option[i].titleKey).subscribe(
            translation => {
              this.Option[i].title = translation;
            },
          (Error) => {
    
          },
          () => {
            this.tourService.initialize(this.Option,
              {
                route: '',
                preventScrolling: false,
              }
            );
          })
          this.translate.get(this.Option[i].contentKey).subscribe(
            translation => {
              this.Option[i].content = translation;
            },
          (Error) => {
    
          },
          () => {
            this.tourService.initialize(this.Option,
              {
                route: '',
                preventScrolling: false,
              }
            );
          })
        }
    }
}