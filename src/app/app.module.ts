import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { PopupModule} from 'ng2-opd-popup';
import { TourService } from 'ngx-tour-ng-bootstrap';
import { AppComponent } from './app.component';
import {HeaderComponent} from './Component/header/header.component';
import { MainScreenComponent } from './Component/main-screen/main-screen.component';
import { OpCode } from './Models/opcode.model';

import {TourNgBootstrapModule} from 'ngx-tour-ng-bootstrap';
import {RouterModule} from '@angular/router';
import {Global} from './global';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClientModule, HttpClient} from '@angular/common/http';

import { TextMaskModule } from 'angular2-text-mask';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainScreenComponent
  ],
  imports: [
    BrowserModule,
    TextMaskModule,
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        component: MainScreenComponent
      },
      {
        path: 'mainscreen',
        component: MainScreenComponent
      },
    ], {useHash: true}),
    PopupModule.forRoot(),
    TourNgBootstrapModule.forRoot(),
    FormsModule,
    HttpModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [TourService, Global],
  bootstrap: [AppComponent]
})
export class AppModule { }
