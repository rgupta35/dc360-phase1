export class Config {
    public resourceId: number;
    public configId: number;
    public configCode: string;
    public configDescription: string;
}