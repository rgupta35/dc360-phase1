export class Resources {
    public resourceID: number;
    public resourceName: string;
    public locID: number;
    public locName: string;
    public deptCode: string;
    public deptName: string;
    public deptType: number;
    public facilityID: number;
    public facilityName: string;
    public pcFriendlyName: string;
    public pcPrimaryMacAddress: string;
    public pcSecondaryMacAddress: string;
}