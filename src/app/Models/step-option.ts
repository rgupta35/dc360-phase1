import { UrlSegment } from '@angular/router';
export interface IStepOption {
    stepId?: string;
    anchorId?: string;
    title?: string;
    content?: string;
    route?: string | UrlSegment[];
    nextStep?: number | string;
    prevStep?: number | string;
    placement?: 'above' | 'below' | 'after' | 'before' | 'left' | 'right';
    preventScrolling?: boolean;
    isClickable?: boolean;
    titleKey?: string;
    contentKey?: string;
  }
  