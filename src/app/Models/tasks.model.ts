export class Tasks {
    public taskID: number;
    public resourceID: number;
    public resourceName: string;
    public startDateTime: string;
    public endDateTime:string;
    public subJobNo:string;
    public taskName:string;
    public company:string;
    public orderNo: number;
    public plannedCopies: number;
    public noUp: number;
    public makeReadyMins: number;
    public runMins = 0;
    public noPasses: number;
    public speed: number;
    public taskStatus:string;
    public netImpressions: number;
    public grossImpressions: number;
    public wasteImpressions: number;
    public netCopies: number;
    public grossCopies: number;
    public wasteCopies: number;
    public qtyToGo: number;
    public totalSubJobQty: number;
    public configId: number;
}