export class CurrentEvent {
   public resourceId: number;
   public taskId: number;
   public opCodeId: number;
   public opUnitId: number;
   public opAreaId: number;
   public configId: number;
   public operatorId: number;
   public assistants: number;
   public eventStartDateTime: string;
}