export class OpCode {
    public opCodeID: number;
    public opUnitID: number;
    public opAreaID: number;
    public opCodeTypeColorRGB: string;
    public opCodeName: string;
    public opCodeTypeName: string;
}