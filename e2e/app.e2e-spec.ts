import { CountAdjustmentSampleProjectPage } from './app.po';

describe('count-adjustment-sample-project App', () => {
  let page: CountAdjustmentSampleProjectPage;

  beforeEach(() => {
    page = new CountAdjustmentSampleProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
