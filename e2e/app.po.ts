import { browser, by, element } from 'protractor';

export class CountAdjustmentSampleProjectPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}
